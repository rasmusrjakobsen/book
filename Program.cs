﻿using Microsoft.Data.SqlClient;
namespace book;
class Program
{
    static void Main(string[] args)
    {
        Book harryPotter1 = new Book("Harry Potter and the Philisophers Stone", "J.K. Rowling", 155.50);
        Book allTheLightWeCannotSee = new Book("All the light we cannot see", "Anthony Doerr", 0.00);
        Book mindfulnessInPlainEnglish = new Book("Mindfulness in Plain English", "Henepola Gunaratana", 37.50);
        string connectionString = "Server=10.56.8.36; Database=DB_F23_09; User Id=DB_F23_USER_09; Password=OPENDB_09; TrustServerCertificate=true";
        Book.Create(allTheLightWeCannotSee);
        Book.Create(mindfulnessInPlainEnglish);

        // using (SqlConnection connection = new SqlConnection(connectionString))
        // {
        //     connection.Open();
        //     SqlCommand command = new SqlCommand("INSERT INTO Book VALUES ('Harry Potter', 'J.K. Rowling', 155)", connection);
        //     // command.Connection.Open();
        //     int i = command.ExecuteNonQuery();
        //     Console.WriteLine(i);
        // }
        // using (SqlConnection connection = new SqlConnection(connectionString))
        // {
        //     connection.Open();
        //     SqlCommand command = new SqlCommand("CREATE TABLE AUTHOR (AuthorId int IDENTITY(1,1) PRIMARY KEY, Name nvarchar(100), Gender nvarchar(100));", connection);
        //     int i = command.ExecuteNonQuery();
        //     Console.WriteLine(i);
        // }
    }
}
