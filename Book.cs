using System.Data;
using Microsoft.Data.SqlClient;
public class Book
{
    public string Title {get; set;}
    public string Author {get; set;}
    public double Price {get; set;}
    public int Id {get; set;}
    static string connectionString = "Server=10.56.8.36; Database=DB_F23_09; User Id=DB_F23_USER_09; Password=OPENDB_09; TrustServerCertificate=true";

    public Book(string Title, string Author, double Price)
    {
        this.Title = Title;
        this.Author = Author;
        this.Price = Price;
    }

    public static void Create(Book book)
    {
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            connection.Open();
            SqlCommand command = new SqlCommand("INSERT INTO Book VALUES(@Title, @Author, @Price)", connection);
            command.Parameters.Add("@Title", SqlDbType.NVarChar).Value = book.Title;
            command.Parameters.Add("@Author", SqlDbType.NVarChar).Value = book.Author;
            command.Parameters.Add("@Price", SqlDbType.Float).Value = book.Price;
            book.Id = Convert.ToInt32(command.ExecuteScalar());
            Console.WriteLine(book.Id);
        }
    }

}

// public void Create(Car carToBeCreated)
//         {
            
//             using (SqlConnection con = new SqlConnection(ConnectionString))
//             {
//                 con.Open();
//                 SqlCommand cmd = new SqlCommand("INSERT INTO Car (Make, Model, Year, Description)" +
//                                                  "VALUES(@Make,@Model,@Year,@Description)" +
//                                                  "SELECT @@IDENTITY", con);
//                 cmd.Parameters.Add("@Make", SqlDbType.NVarChar).Value = carToBeCreated.Make;
//                 cmd.Parameters.Add("@Model", SqlDbType.NVarChar).Value = carToBeCreated.Model;
//                 cmd.Parameters.Add("@Year", SqlDbType.Int).Value = carToBeCreated.Year;
//                 cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = carToBeCreated.Description;
//                 carToBeCreated.Id = Convert.ToInt32(cmd.ExecuteScalar());
//                 cars.Add(carToBeCreated);
//             }
//         }